export interface IoEventInfo<T = string | Object> {
  name: string,
  /**
   * we will be using "count" as a way of knowing if it has been triggered.
   */
  count?: number;

  /**
   * Aka unique event
   */
  once?: boolean;

  /**
   * Use an initial state if you need one
   */
  initialState?: T
}

export interface SocketState {
  connected: boolean,
  id?: string;
}
