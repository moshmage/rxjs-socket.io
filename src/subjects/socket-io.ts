import {ioEvent} from "./io-events";
import {IoEventInfo, SocketState} from '../interfaces/socket-io';
import * as io from 'socket.io-client';
import {Observable, Subject, Subscription} from 'rxjs';

/** @private */
const SOCKET_URL = "http://localhost:5000";

export class IO {
    /** this will be set as a reference to io.Socket */
    private socket: SocketIOClient.Socket;

    /**
     * events will be used to push which events we should be listening to
     * @private
     * @type ioEvent[]
     */
    private events: ioEvent[] = [];

    /**
     * @type {Subject<SocketState>}
     * @private
     */
    private _socketState = new Subject<SocketState>();

    /**
     * The Subscribable prop
     * subscribe to this prop to be notified of data update
     * @type {Observable<SocketState>}
     * @readonly
     */
    public readonly event$: Observable<SocketState> = this._socketState.asObservable();

    /** this prop will pretty much control the "is connected" or not.
     * it also controls whether or not we should issue this.socket.disconnect() */
    private _connected: boolean = false;

    /**
     * returns an {@link ioEvent} by matching ioEvent.name against the provided argument string
     * @param name {string} `the name of the event`
     * @param isUnique {boolean}
     * @returns {ioEvent | boolean}
     */
    private getEvent(name: string, isUnique?: boolean) {
        return this.events.find(ioEvent => isUnique === ioEvent.isUnique && name === ioEvent.name);
    }

    /**
     * a reference to the raw socket returned from io(), if connected
     * @returns {SocketIOClient.Socket}
     */
    public get raw(): SocketIOClient.Socket { return this.socket }

    /** an alias for {@link raw | socket}.emit()
     * which will only emit if connected. */
    public emit(eventName: string, data?: Object): void {
        if (this.connected) {
            this.socket.emit(eventName, data);
        }
    }

    /**
     * check if {@link ioEvent} exists so we don't pollute the events list with dupes
     * EVEN if it's a `once` event, as one change will trigger all listeners
     * @param ioEvent {ioEvent}
     * @return {boolean}
     */
    public eventExists(ioEvent :ioEvent): boolean {
        return this.events.some(_ioEvent => {
            if (!_ioEvent.hasTriggered && !ioEvent.hasTriggered && ioEvent.isUnique &&
                _ioEvent.name === ioEvent.name) return false;

            return !_ioEvent.isUnique && _ioEvent.name === ioEvent.name;
        });
    }

    /**
     * pushes an ioEvent to be heard and returns the event,
     * or the existing event - if that's true
     * @usage
     *
     * ```typescript
     * const helloWorld = new ioEvent('hello-world);
     * const helloWorld$ = this.listenToEvent(helloWorld).event$;
     *
     * helloWorld$.subscribe(newState => console.log(newState));
     * ```
     * @param ioEvent {ioEvent<T>}
     * @returns {ioEvent<T>}
     */
    public listenToEvent<T>(ioEvent: ioEvent<T>):ioEvent<T> {
        if (!this.eventExists(ioEvent)) {
            this.events.push(ioEvent);
            if (this.connected) ioEvent.hook(this.socket)
        }
        else ioEvent = this.getEvent(ioEvent.name, ioEvent.isUnique);
        return ioEvent;
    }

    /**
     * A function that receives an array of either strings or IoEventInfo
     * and returns the equivalent subscription after having transformed the
     * input into a @ioEvent and having issued `this.listenToEvent`.
     *
     * Think of this as a bulk listenToEvent that can be deconstructed.
     *
     * @usage
     *```typescript
     * const [helloWorld$, fooBar$] = {@link listen | this.socket.listen}([
     *   'hello-world',
     *   {name: 'foo-bar', once: true}
     * ]);
     *
     * helloWorld$.subscribe(newState => console.debug('helloWorld$',newState));
     * fooBar$.subscribe(newState => console.debug('fooBar$', newState));
     *```
     * @param eventsArray {(string|IoEventInfo)[]}
     * @returns {Subscription[]}
     */
    public listen(eventsArray: Array<string|IoEventInfo>) {
        return eventsArray.map((event:string|IoEventInfo) => {
            let _event: ioEvent;
            if (typeof event !== 'string') {
                let type = <IoEventInfo>event;
                _event = new ioEvent(type.name, type.once, type.count);
            } else _event = new ioEvent(event);

            return this.listenToEvent(_event).event$;
        });
    };

    /**
     * Removes an {@link ioEvent} from the listening queue
     * @param ioEventName {string}
     */
    public unhook(ioEventName: string) {
        const index = this.events.findIndex(({name}) => name === ioEventName);
        if (index === -1) return;
        this.events[index].unhook();
        this.events.splice(index, 1);
    }

    /**
     * Makes a new connection to the provided address and sets up a `on connect` by default
     * which will in turn update the {@link event$} Subject with the containing
     * the received data as an argument (as well as the event-name)
     * @param address {String}     defaults to "http://localhost:5000"
     * @param forceNew {Boolean}
     */
    public connect(address?: string, forceNew?:boolean) :void {
        if (this.connected && !forceNew) return;
        else if (this.connected && forceNew) this.connected = false;

        this.socket = io(address || SOCKET_URL);
        this.socket.on('connect', () => {
            this._connected = true;

            this._socketState.next({connected: true, id: this.socket.id || '0' });
            this.events.forEach(ioEvent => {
                /** this is where we hook our previously new()ed {@link ioEvent}s to the socket.
                 * This is so we can have one listener per event. as opposed to one event
                 * to all listeners
                 * */
                ioEvent.hook(this.socket);
            });

            this.socket.on('disconnect', () => {
                this.connected = false;
                /** call reset state on disconnection */
                this.events.forEach(ioEvent => ioEvent.initialState !== undefined && ioEvent.resetState());
            })
        });
    };

    /**
     * check if socket is connected
     * @returns {boolean}
     */
    public get connected() {return this._connected; }

    /**
     * Only works as a setter if {@param value} = false and {@link connected} = true
     * @param value {boolean}
     */
    public set connected(value: boolean) {
        if (!value && this._connected)
            this.disconnect();
    };

    /**
     * Disconnects the {@link raw | Socket} connection
     * @void
     */
    public disconnect(): void {
        this.socket.disconnect();
        this._connected = false;
        this._socketState.next({connected: false});
    }

}
