/**
 * Created by Mosh Mage on 12/17/2016.
 */
import {IO} from './subjects/socket-io';
import {ioEvent} from './subjects/io-events';
import {Observable, Subject} from 'rxjs';

describe('IO', () => {
    it ('is instance of itself', () => {
        let socket = new IO();
        expect(socket instanceof IO).toBe(true);
    });
    describe('listenEvent & eventExists', () => {
        const socket = new IO();
        const eventCount = 0;
        const event = new ioEvent('test-event');
        const uniqueEvent = new ioEvent('test-event', true);

        it('event does not exist', () => {
            const newEvent = new ioEvent('new-event');
            expect(socket.eventExists(newEvent)).toBeFalsy();
        });

        it('it returns the same event, because it already exists', () => {
            const sameEvent = socket.listenToEvent(event);
            expect(event).toEqual(sameEvent);
            expect(socket.eventExists(event)).toBeTruthy();
        });

        it('listens because its unique', () => {
            const uniqueHook = socket.listenToEvent(uniqueEvent);
            expect(uniqueEvent).toBe(uniqueHook);
        });

        it('returns the same unique event', () => {
            const uniqueHook = socket.listenToEvent(uniqueEvent);
            const [anotherUniqueListen] = socket.listen([uniqueEvent]);
            expect(anotherUniqueListen).toEqual(uniqueHook.event$);
        });

        it('makes listen create a new ioEvent and hook it because mock connected and then unhook it after', () => {
            const _socket = new IO();
            Object.assign(_socket, {socket: {on:() => {}, off: () => {}}})
            spyOnProperty(_socket, 'connected', 'get').and.returnValue(true);

            const [event] = _socket.listen(['new-event']);
            expect(event).toBeInstanceOf(Observable);
            _socket.unhook('new-event');
            _socket.unhook('not-found');
        });

        describe('mocks connection', () => {
            it('no forcing logic', () => {
                const _socket = new IO();
                spyOn(_socket, 'connect').and.callThrough();
                _socket.connect();
            });
        })

    });
    describe('Public coverage', () => {

        it("raw", () => {
            const spySocket = new IO();
            expect(spySocket.raw).toBeFalsy();
            // @ts-ignore
            spyOn(spySocket, 'connected').and.returnValue(true);
            expect(spySocket.raw).toBeUndefined(); /** is undefined because .connect() wasn't issued */

        });

        it('socketState updating to true', () => {
            const spySocket = new IO();
            const event$ = spySocket.event$;
            spySocket.connected = true;

            event$.subscribe((newValue) => {
                expect(newValue).toEqual({connected: true});
            });
        });

        it ('SocketState updating to false', () => {
            const spySocket = new IO();
            const event$ = spySocket.event$;
            Object.assign(spySocket, {
                _connected: true,
                socket: {disconnect() {}}
            });
            spyOn((spySocket as any).socket, 'disconnect').and.callThrough();

            event$.subscribe((newValue) => {
                expect({connected: false}).toEqual(newValue);
            }).unsubscribe();

            spySocket.connected = false;
        });

        it('Emit', () => {
            const spySocket = new IO();
            Object.assign(spySocket, {socket: {emit() {}}});

            spyOn((spySocket as any).socket, 'emit').and.callThrough();
            spyOnProperty(spySocket as any, 'connected').and.returnValue(true);

            spySocket.emit('test-event',{});
            expect((spySocket as any).socket.emit.calls.count()).toBe(1);

        });
    });
});

describe('ioEvent', () => {
    const socket = new IO();
    const eventCount = 0;
    let event;
    describe('Public coverage', () => {
        beforeEach(() => {
            event = new ioEvent('test-event');
            Object.assign(event, {clientSocket: {once() {}, on() {}, off() {} }})
        });

        it('hooks, once', () => {
            event.event.once = true;
            spyOn(event.clientSocket, 'once').and.callThrough();
            event.hook(event.clientSocket);
            expect(event.clientSocket.once.calls.count()).toBe(1);
        });

        it('hooks using on', () => {
            spyOn(event.clientSocket, 'on').and.callThrough();
            event.hook(event.clientSocket);
            expect(event.clientSocket.on.calls.count()).toBe(1);
        });

        it('hooks using on with a new socket', () => {
            let newClientSocket = {clientSocket: {once() {}, on() {}, off() {}, id: 1}};
            spyOn(newClientSocket.clientSocket, 'on').and.callThrough();
            spyOn(event.clientSocket, 'on').and.callThrough();

            event.hook(event.clientSocket);
            event.hook(newClientSocket.clientSocket);

            expect(event.clientSocket.on.calls.count()).toBe(1);
            expect((newClientSocket.clientSocket.on as any).calls.count()).toBe(1);
        });

        it('does not unhook because once', () => {
            event.event.once = true;
            spyOn(event.clientSocket, 'off').and.callThrough();
            event.unhook();
            expect(event.clientSocket.off.calls.count()).toBe(0);
        });

        it('does unhook', () => {
            spyOn(event.clientSocket, 'off').and.callThrough();
            event.unhook();
            expect(event.clientSocket.off.calls.count()).toBe(1);
        });

        it('sets a callback function', () => {
            /** check the "throw" further down for its counter */
            let noop = () => {};
            event.onUpdate = noop;
            expect(event.onUpdate).toBe(noop);
        });

        describe('initialState', () => {
            it ('is undefined', () => {
                expect(event.initialState).toBe(undefined);
            });

            it('is assignable and object', () => {
                event.initialState = {hello: 'world'};
                event.initialState = {world: 'hello'};
                expect(event.initialState).toEqual({hello: 'world', world: 'hello'});
            });

            it('is assignable and not a object, so rewritten', () => {
                event.initialState = 'hello';
                expect(event.initialState).toEqual('hello');
            });

            it('resets state (and cover onUpdateData on the way)', () => {
                const noop = () => {};
                event.onUpdate = noop;
                event.resetState();
                expect(event.initialState).toBe(undefined);
            })
        })
    });

    describe('throws', () => {
        it('Should throw because a socketClient wasnt provided', () => {
            event = new ioEvent('test-event');
            expect(function () { return event.hook(); }).toThrowError(/no socket/i);
        });

        it ('should throw because provided is not a function', () => {
            event = new ioEvent('test-event', false, 0);
            expect(function () {
                return event.onUpdate = '';
            }).toThrowError(/type Function/i);
        });
    })
});
